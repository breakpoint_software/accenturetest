﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SearchText
{
    class Program
    {
        static void Main(string[] args)
        {

            // c: \users\ssegovia\documents\visual studio 2015\Projects\SearchText\SearchText\Files\
            Console.WriteLine(@"Search folder -> ");
            string folder = Console.ReadLine();
            string fileContent = null;
            Regex regex = new Regex(@"\b\d{3}-\d{4}\b|\b\d{3}-\d{3}-\d{4}\b|\(\d{3}\)\s\d{3}-\d{4}");
            Console.WriteLine("Files with phone numbers: ");
            foreach (string fileName in Directory.GetFiles(folder, "*.html", SearchOption.AllDirectories))
            {
                fileContent = File.ReadAllText(fileName);
                if (regex.IsMatch(fileContent))
                {
                    Console.WriteLine(fileName);
                }
            }
            Console.Read();
        }
    }
}
