function count_chars(phrase) {
	
	var result = {};
	for(i = 0; i < phrase.length; i++) {
		console.log(phrase[i]);
		if (typeof result[phrase[i]] == 'undefined') {
			result[phrase[i]] = 0;
		}
		result[phrase[i]]++;
	}
  console.log("Result => " + JSON.stringify(result));
  //document.getElementById('result').value = JSON.stringify(result);
	return result;
}


function get_hammingDistance(phrase1, phrase2) {
	if (phrase1.length != phrase2.length)
		return -1;
	
	var result = 0;
	for(i = 0; i < phrase1.length; i++) {
		console.log(phrase1[i] +'='+ phrase2[i] );
		if (phrase1[i] != phrase2[i]) {
			result++;
		}
		
	}
	console.log("Result => " + JSON.stringify(result));
  //document.getElementById('result').value = JSON.stringify(result);
	return result;
}

	
function test() {
    count_chars('cat');
    get_hammingDistance('cats', 'dogs');
}